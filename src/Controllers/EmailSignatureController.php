<?php

namespace FDP\Signatures\Controllers;

use FDP\Signatures\Models\EmailSignature;
use FDP\Common\Utilities\MinifiedResponse;

use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\View\Requirements;

class EmailSignatureController extends Controller
{
    private static $url_handlers = [
        '$Slug' => 'Index'
    ];
    protected $templates = [
        'index' => ['EmailSignatureController']
    ];

    public function init()
    {
        parent::init();
        $this->setResponse(new MinifiedResponse());
    }

    public function Index(HTTPRequest $request)
    {
        if (($slug = $request->param('Slug')) && !empty($slug)) {
            if ($signature = EmailSignature::get()->filter('Slug', $slug)->first()) {
                return ['Signature' => $signature];
            }
        }
        return [];
    }
}
