<?php

namespace FDP\Signatures\ModelAdmins;

use FDP\Signatures\GridField\EmailSignatureItemRequest;
use FDP\Signatures\Models\EmailSignature;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridFieldDetailForm;

class EmailSignatureAdmin extends ModelAdmin
{
    private static $menu_title = 'Signatures';
    private static $url_segment = 'signatures';
    private static $menu_icon = 'fdp/signatures:images/signature-admin.svg';
    private static $managed_models = [
        EmailSignature::class => [
            'title' => 'Signatures'
        ]
    ];

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        if ($field = $form->Fields()->dataFieldByName($this->sanitiseClassName(EmailSignature::class))) {
            $field->getConfig()->getComponentByType(GridFieldDetailForm::class)
                ->setItemRequestClass(EmailSignatureItemRequest::class);
        }
        return $form;
    }
}
