<?php

namespace FDP\Signatures\GridField;

use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\GridField\GridFieldDetailForm_ItemRequest;

class EmailSignatureItemRequest extends GridFieldDetailForm_ItemRequest
{
    private static $allowed_actions = [
        'edit',
        'view',
        'ItemEditForm',
        'sendEmailLink'
    ];

    protected function getFormActions()
    {
        $actions = parent::getFormActions();
        if ($signature = $this->record) {
            if ($signature->ID > 0) {
                $actions->push(
                    FormAction::create('sendEmailLink', 'Send Link by Email')
                        ->setUseButtonTag(true)
                        ->addExtraClass('btn-secondary font-icon-upload')
                );
            }
        }
        return $actions;
    }

    public function sendEmailLink($data, $form)
    {
        if ($signature = $form->getController()->getRecord()) {
            $signature->sendEmailLink();
        }
        return $this->redirectAfterSave(false);
    }
}
