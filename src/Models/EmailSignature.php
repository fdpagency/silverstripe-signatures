<?php

namespace FDP\Signatures\Models;

use FDP\Common\Utilities\StringUtilities;

use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Manifest\ModuleResourceLoader;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\View\Requirements;

class EmailSignature extends DataObject
{
    private static $table_name = 'EmailSignature';
    private static $singular_name = 'Signature';
    private static $plural_name = 'Signatures';

    private static $db = [
        'Name' => 'Varchar(200)',
        'Slug' => 'Varchar(200)',
        'Email' => 'Varchar(254)'
    ];
    private static $default_sort = 'Name ASC';
    private static $summary_fields = [
        'Name' => 'Name',
        'Email' => 'Email'
    ];

    public function getCMSFields()
    {
        $fields = FieldList::create(
            TextField::create('Name', 'Name'),
            EmailField::create('Email', 'Email')
        );
        if ($this->ID > 0) {
            $fields->push(LiteralField::create(
                'SignatureLink',
                "<p><a href=\"{$this->Link()}\" target=\"_blank\">Click here</a> to view this signature</p>"
            ));
        }
        $this->extend('updateCMSFields', $fields);
        return $fields;
    }

    protected function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if ($this->isChanged('Name')) {
            $this->Slug = StringUtilities::form_slug($this->Name);
        }
    }

    public function forTemplate()
    {
        Requirements::clear();
        return $this->renderWith('EmailSignature');
    }

    private static $template_vars = [
        'black' => '#000000',
        'white' => '#ffffff',

        'table-attrs' => 'cellpadding="0" cellspacing="0"',
        'image-attrs' => 'style="display: block; border: none;"',
        'link-attrs' => 'style="border: none; outline: none; text-decoration: none"'
    ];

    public function Var($name)
    {
        return DBField::create_field('HTMLText', Config::inst()->get(get_class($this), 'template_vars')[$name]);
    }

    private $spacer_image;

    public function Spacer($width = 0, $height = 0, $raw = false)
    {
        if (is_null($this->spacer_image)) {
            $this->spacer_image = ModuleResourceLoader::resourceURL('fdp/signatures:images/spacer.gif');
        }
        $html = sprintf(
            '<img style="display: block; border: none;" src="%s" width="%s" height="%s" />',
            $this->spacer_image,
            $width,
            $height
        );
        return $raw ? $html : DBField::create_field('HTMLText', $html);
    }

    public function SpacerCell($width = 0, $height = 0)
    {
        return DBField::create_field(
            'HTMLText',
            "<td width=\"{$width}\" height=\"{$height}\">{$this->Spacer($width, $height, true)}</td>"
        );
    }

    public function Link($action = null)
    {
        return Director::absoluteURL("signatures/{$this->Slug}{$action}");
    }

    public function sendEmailLink()
    {
        $email = $this->extend('getSignatureEmail')[0];
        $email->send();
    }
}
